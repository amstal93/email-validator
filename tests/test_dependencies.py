# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import uuid
from collections import namedtuple
from ipaddress import IPv4Address, IPv6Address
from typing import Union
from unittest.mock import patch

import requests
from fastapi import status
from healthcheck.healthcheck import HealthCheck
from hypothesis import given
from hypothesis.strategies import integers, ip_addresses
from opencensus.ext.azure.log_exporter import AzureLogHandler
from opencensus.ext.azure.trace_exporter import AzureExporter
from zerobouncesdk import zerobouncesdk
from zerobouncesdk.zb_get_credits_response import ZBGetCreditsResponse

from email_validator import dependencies
from email_validator.settings import (
    AppInsightsSettings,
    HealthCheckSettings,
    ZeroBounceSettings,
)
from tests.utils import STUB_ZEROBOUNCE_API_KEY, clear_caches, get_stub_logger

ZEROBOUNCE_CREDITS_THRESHOLD = 100

################################################################################
# Logger and Tracer
################################################################################


def test_that_logger_contains_azure_handler_when_instrumentation_key_is_provided():
    # Arrange
    clear_caches()
    appinsights_settings = AppInsightsSettings(instrumentation_key=uuid.UUID(int=0))
    # Act
    logger = dependencies.get_logger(appinsights_settings)
    # Assert
    azure_log_handlers = filter(
        lambda h: isinstance(h, AzureLogHandler), logger.handlers
    )
    assert any(azure_log_handlers)


def test_that_logger_does_not_contain_azure_handler_when_instrumentation_key_is_not_provided():
    # Arrange
    clear_caches()
    appinsights_settings = AppInsightsSettings(instrumentation_key=None)
    # Act
    logger = dependencies.get_logger(appinsights_settings)
    # Assert
    azure_log_handlers = filter(
        lambda h: isinstance(h, AzureLogHandler), logger.handlers
    )
    assert not any(azure_log_handlers)


# pylint: disable=line-too-long, C0301
def test_that_logger_contains_only_one_azure_handler_when_instrumentation_key_is_provided_and_get_logger_is_called_twice():
    # Arrange
    clear_caches()
    appinsights_settings = AppInsightsSettings(instrumentation_key=uuid.UUID(int=0))
    # Act
    dependencies.get_logger(appinsights_settings)
    logger = dependencies.get_logger(appinsights_settings)
    # Assert
    azure_log_handlers = filter(
        lambda h: isinstance(h, AzureLogHandler), logger.handlers
    )
    assert len(list(azure_log_handlers)) == 1


def test_that_tracer_has_azure_exporter_when_instrumentation_key_is_provided():
    # Arrange
    clear_caches()
    appinsights_settings = AppInsightsSettings(instrumentation_key=uuid.UUID(int=0))
    # Act
    tracer = dependencies.get_tracer(appinsights_settings)
    # Assert
    assert isinstance(tracer.exporter, AzureExporter)


def test_that_tracer_does_not_have_azure_exporter_when_instrumentation_key_is_not_provided():
    # Arrange
    clear_caches()
    appinsights_settings = AppInsightsSettings(instrumentation_key=None)
    # Act
    tracer = dependencies.get_tracer(appinsights_settings)
    # Assert
    assert not isinstance(tracer.exporter, AzureExporter)


################################################################################
# Health checks
################################################################################


FakeResponse = namedtuple("FakeResponse", "status_code")


@given(website=ip_addresses())
def test_that_health_check_returns_200_when_ping_website_check_succeeds(
    website: Union[IPv4Address, IPv6Address]
):
    # Arrange
    clear_caches()
    health_check_settings = HealthCheckSettings(ping_websites=[str(website)])
    with patch.object(
        requests,
        "head",
        return_value=FakeResponse(status_code=status.HTTP_200_OK),
    ):
        # Act
        (_, status_code, _) = dependencies.get_health_check(
            get_stub_logger(),
            health_check_settings,
            dependencies.get_zerobounce_settings(),
        ).run()
        # Assert
        assert status_code == status.HTTP_200_OK


@given(website=ip_addresses())
def test_that_health_check_returns_503_when_ping_website_check_fails(
    website: Union[IPv4Address, IPv6Address]
):
    # Arrange
    clear_caches()
    health_check_settings = HealthCheckSettings(ping_websites=[str(website)])
    with patch.object(
        requests,
        "head",
        return_value=FakeResponse(status_code=status.HTTP_503_SERVICE_UNAVAILABLE),
    ):
        # Act
        (_, status_code, _) = dependencies.get_health_check(
            get_stub_logger(),
            health_check_settings,
            dependencies.get_zerobounce_settings(),
        ).run()
        # Assert
        assert status_code == status.HTTP_503_SERVICE_UNAVAILABLE


def test_that_health_check_includes_zerobounce_credits_check_when_zerobounce_is_available():
    # Arrange
    clear_caches()
    health_check_settings = HealthCheckSettings(ping_websites=[])
    zerobounce_settings = ZeroBounceSettings(api_key=STUB_ZEROBOUNCE_API_KEY)
    with patch.object(HealthCheck, "add_check") as mock:
        # Act
        dependencies.get_health_check(
            get_stub_logger(),
            health_check_settings,
            zerobounce_settings,
        )
        # Assert
        mock.assert_called_once()


def test_that_health_check_does_not_includes_zerobounce_credits_check_when_zerobounce_is_not_available():
    # Arrange
    clear_caches()
    health_check_settings = HealthCheckSettings(ping_websites=[])
    zerobounce_settings = ZeroBounceSettings(api_key=None)
    with patch.object(HealthCheck, "add_check") as mock:
        # Act
        dependencies.get_health_check(
            get_stub_logger(),
            health_check_settings,
            zerobounce_settings,
        )
        # Assert
        mock.assert_not_called()


@given(available_credits=integers().filter(lambda i: i >= ZEROBOUNCE_CREDITS_THRESHOLD))
def test_that_health_check_returns_200_when_zerobounce_credits_check_succeeds(
    available_credits: int,
):
    # Arrange
    clear_caches()
    health_check_settings = HealthCheckSettings(ping_websites=[])
    zerobounce_settings = ZeroBounceSettings(
        api_key=STUB_ZEROBOUNCE_API_KEY, credits_threshold=ZEROBOUNCE_CREDITS_THRESHOLD
    )
    zerobounce_response = ZBGetCreditsResponse(f'{{"Credits":{available_credits}}}')
    with patch.object(zerobouncesdk, "get_credits", return_value=zerobounce_response):
        # Act
        (_, status_code, _) = dependencies.get_health_check(
            get_stub_logger(),
            health_check_settings,
            zerobounce_settings,
        ).run()
        # Assert
        assert status_code == status.HTTP_200_OK


@given(available_credits=integers().filter(lambda i: i < ZEROBOUNCE_CREDITS_THRESHOLD))
def test_that_health_check_returns_503_when_zerobounce_credits_check_fails(
    available_credits: int,
):
    # Arrange
    clear_caches()
    health_check_settings = HealthCheckSettings(ping_websites=[])
    zerobounce_settings = ZeroBounceSettings(
        api_key=STUB_ZEROBOUNCE_API_KEY, credits_threshold=ZEROBOUNCE_CREDITS_THRESHOLD
    )
    zerobounce_response = ZBGetCreditsResponse(f'{{"Credits":{available_credits}}}')
    with patch.object(zerobouncesdk, "get_credits", return_value=zerobounce_response):
        # Act
        (_, status_code, _) = dependencies.get_health_check(
            get_stub_logger(),
            health_check_settings,
            zerobounce_settings,
        ).run()
        # Assert
        assert status_code == status.HTTP_503_SERVICE_UNAVAILABLE


################################################################################
# Settings
################################################################################


def test_that_security_settings_are_not_none():
    # Arrange
    clear_caches()
    # Act
    result = dependencies.get_security_settings()
    # Assert
    assert result is not None
