# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import string
import uuid
from logging import Logger
from unittest.mock import patch

import pytest
from fastapi import status
from fastapi.testclient import TestClient
from hypothesis import given
from hypothesis.strategies import emails, text
from zerobouncesdk import zerobouncesdk

from email_validator import __version__, dependencies, main, validator
from email_validator.dependencies import API_KEY_HEADER_NAME, get_logger
from email_validator.models import AddressValidationResult, Validator, Verdict
from email_validator.settings import (
    ApiKey,
    AppInsightsSettings,
    SecuritySettings,
    ZeroBounceSettings,
)
from tests.utils import (
    STUB_ZEROBOUNCE_API_KEY,
    AnyArg,
    clear_caches,
    get_stub_logger,
    is_parsable_address,
)

client = TestClient(main.app)

main.app.dependency_overrides[get_logger] = get_stub_logger

################################################################################
# Custom Uvicorn worker
################################################################################


def test_that_custom_uvicorn_worker_config_is_a_dict():
    # Act & Assert
    assert isinstance(main.CustomUvicornWorker.CONFIG_KWARGS, dict)


################################################################################
# Startup
################################################################################


@pytest.mark.asyncio
async def test_that_startup_logs_about_appinsights_availability_when_available():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_appinsights_settings",
        return_value=AppInsightsSettings(instrumentation_key=uuid.UUID(int=0)),
    ), patch.object(
        Logger,
        "info",
    ) as mock:
        # Act
        await main.on_startup()
        # Assert
        mock.assert_called_once()


@pytest.mark.asyncio
async def test_that_startup_does_not_log_about_appinsights_availability_when_not_available():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_appinsights_settings",
        return_value=AppInsightsSettings(instrumentation_key=None),
    ), patch.object(
        Logger,
        "info",
    ) as mock:
        # Act
        await main.on_startup()
        # Assert
        mock.assert_not_called()


@pytest.mark.asyncio
async def test_that_startup_logs_about_zerobounce_availability_when_available():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_zerobounce_settings",
        return_value=ZeroBounceSettings(api_key=STUB_ZEROBOUNCE_API_KEY),
    ), patch.object(zerobouncesdk, "initialize",), patch.object(
        Logger,
        "info",
    ) as mock:
        # Act
        await main.on_startup()
        # Assert
        mock.assert_called_once()


@pytest.mark.asyncio
async def test_that_startup_does_not_log_about_zerobounce_availability_when_not_available():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_zerobounce_settings",
        return_value=ZeroBounceSettings(),
    ), patch.object(zerobouncesdk, "initialize",), patch.object(
        Logger,
        "info",
    ) as mock:
        # Act
        await main.on_startup()
        # Assert
        mock.assert_not_called()


@pytest.mark.asyncio
async def test_that_startup_initializes_zerobounce_when_available():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_zerobounce_settings",
        return_value=ZeroBounceSettings(api_key=STUB_ZEROBOUNCE_API_KEY),
    ), patch.object(Logger, "info",), patch.object(
        zerobouncesdk,
        "initialize",
    ) as mock:
        # Act
        await main.on_startup()
        # Assert
        mock.assert_called_once()


@pytest.mark.asyncio
async def test_that_startup_does_not_initialize_zerobounce_when_not_available():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_zerobounce_settings",
        return_value=ZeroBounceSettings(api_key=None),
    ), patch.object(Logger, "info",), patch.object(
        zerobouncesdk,
        "initialize",
    ) as mock:
        # Act
        await main.on_startup()
        # Assert
        mock.assert_not_called()


################################################################################
# Redirects
################################################################################


@pytest.mark.parametrize("endpoint", ["/", "/redoc", "/swagger"])
def test_that_common_docs_endpoint_redirects_to_openapi_docs(endpoint):
    # Act
    response = client.get(endpoint, allow_redirects=False)
    # Assert
    assert response.is_redirect
    assert response.headers["Location"].endswith("/docs")


################################################################################
# Health checks, version and logs
################################################################################


class FakeHealthCheck:
    def __init__(self, content, status_code):
        self.content = content
        self.status_code = status_code

    def run(self):
        return self.content, self.status_code, {"Content-Type": "text/plain"}


@given(content=text(alphabet=string.ascii_letters))
def test_that_health_endpoint_triggers_health_checks(content):
    # Arrange
    clear_caches()
    status_code = status.HTTP_200_OK
    with patch.object(
        dependencies,
        "__get_health_check",
        return_value=FakeHealthCheck(content, status_code),
    ):
        # Act
        response = client.get("/health")
        # Assert
        assert response.text == content
        assert response.status_code == status_code


def test_that_version_endpoint_responds_version():
    # Act
    response = client.get("/version")
    # Assert
    assert response.json() == __version__


################################################################################
# Validation
################################################################################


@given(address=emails().filter(is_parsable_address))
def test_that_validate_without_security_responds_200(address: str):
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_security_settings",
        return_value=SecuritySettings(enabled=False),
    ), patch.object(
        validator,
        "validate",
        return_value=AddressValidationResult(
            address=address,
            validator=Validator.PY3_VALIDATE_EMAIL,
            verdict=Verdict.VALID,
        ),
    ) as mock:
        # Act
        response = client.get(
            f"/api/v1/validate?address={address}",
            params={"address": address},
        )
        # Assert
        mock.assert_called_once_with(address, AnyArg(), AnyArg(), AnyArg())
        assert response.status_code == status.HTTP_200_OK


@given(address=emails().filter(is_parsable_address))
def test_that_validate_with_security_without_api_key_responds_403(address: str):
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_security_settings",
        return_value=SecuritySettings(enabled=True),
    ):
        # Act
        response = client.get(
            f"/api/v1/validate?address={address}",
            params={"address": address},
        )
        # Assert
        assert response.status_code == status.HTTP_403_FORBIDDEN


@given(
    address=emails().filter(is_parsable_address),
    api_key=text(alphabet=string.ascii_letters, min_size=1),
)
def test_that_validate_with_security_with_invalid_api_key_responds_401(
    address: str, api_key: str
):
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_security_settings",
        return_value=SecuritySettings(enabled=True),
    ):
        # Act
        response = client.get(
            f"/api/v1/validate?address={address}",
            params={"address": address},
            headers={API_KEY_HEADER_NAME: api_key},
        )
        # Assert
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


@given(
    address=emails().filter(is_parsable_address),
    api_key=text(alphabet=string.ascii_letters, min_size=1),
)
def test_that_validate_with_security_with_valid_api_key_responds_200(
    address: str, api_key: str
):
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_security_settings",
        return_value=SecuritySettings(
            enabled=True, api_keys=[ApiKey(name="test", value=api_key)]
        ),
    ), patch.object(
        validator,
        "validate",
        return_value=AddressValidationResult(
            address=address,
            validator=Validator.PY3_VALIDATE_EMAIL,
            verdict=Verdict.VALID,
        ),
    ) as mock:
        # Act
        response = client.get(
            f"/api/v1/validate?address={address}",
            params={"address": address},
            headers={API_KEY_HEADER_NAME: api_key},
        )
        # Assert
        mock.assert_called_once_with(address, AnyArg(), AnyArg(), AnyArg())
        assert response.status_code == status.HTTP_200_OK
