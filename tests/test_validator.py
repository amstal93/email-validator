# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import pydoc
from typing import Type, cast
from unittest.mock import patch

import pymailcheck
import validate_email
from hypothesis import given
from hypothesis.strategies import booleans, emails, integers, sampled_from, text
from validate_email.email_address import EmailAddress
from validate_email.exceptions import DNSTimeoutError, EmailValidationError
from zerobouncesdk import zerobouncesdk
from zerobouncesdk.zb_validate_response import ZBValidateResponse
from zerobouncesdk.zb_validate_status import ZBValidateStatus
from zerobouncesdk.zb_validate_sub_status import ZBValidateSubStatus

from email_validator import validator
from email_validator.dependencies import get_validator_settings, get_zerobounce_settings
from email_validator.models import Validator, Verdict
from email_validator.settings import ValidatorSettings, ZeroBounceSettings
from tests.utils import (
    STUB_ZEROBOUNCE_API_KEY,
    clear_caches,
    get_stub_logger,
    is_parsable_address,
)

DOMAINS_WITH_SUGGESTION = {
    "gmial.com": "gmail.com",
    "autlook.com": "outlook.com",
    "iahoo.com": "yahoo.com",
    "example.con": "example.com",
    "love.com": "live.com",
    "ticcali.it": "tiscali.it",
    "libero.itt": "libero.it",
}


class UnhandledValidatorError(EmailValidationError):
    pass


################################################################################
# Address, account, domain
################################################################################


@given(address=emails().filter(is_parsable_address))
def test_that_validate_with_email_address_parses_address_user_domain(address: str):
    # Arrange
    clear_caches()
    with patch.object(validate_email, "validate_email_or_fail", return_value=True):
        lower_address = address.lower()
        parsed_address = EmailAddress(lower_address)
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        assert result.address == lower_address
        assert result.user == parsed_address.user
        assert result.domain == parsed_address.domain


@given(
    plain_text=text().filter(lambda t: "@" not in t)
)  # Avoids potential email addresses.
def test_that_validate_with_plain_text_does_not_parse_address_user_domain(
    plain_text: str,
):
    # Arrange
    clear_caches()
    # Act
    result = validator.validate(
        plain_text,
        get_stub_logger(),
        get_validator_settings(),
        get_zerobounce_settings(),
    )
    # Assert
    assert result.address == plain_text.lower()
    assert result.user is None
    assert result.domain is None


################################################################################
# Verdict (valid, risky, invalid)
################################################################################


@given(address=emails().filter(is_parsable_address))
def test_that_validate_with_valid_address_returns_valid_verdict(address: str):
    # Arrange
    clear_caches()
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        assert mock.call_count == 2
        assert result.validator is Validator.PY3_VALIDATE_EMAIL
        assert result.verdict is Verdict.VALID
        assert result.reason is None


@given(address=emails().filter(is_parsable_address))
def test_that_validate_with_risky_address_returns_risky_verdict(address: str):
    # Arrange
    clear_caches()
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=None
    ) as mock:
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        assert mock.call_count == 1
        assert result.validator is Validator.PY3_VALIDATE_EMAIL
        assert result.verdict is Verdict.RISKY
        assert result.reason is None


@given(
    address=emails().filter(is_parsable_address),
    verdict_rule_key=sampled_from(sorted(validator.VERDICT_RULES.keys())),
)
def test_that_validate_with_invalid_address_returns_expected_verdict(
    address: str, verdict_rule_key: str
):
    # Arrange
    clear_caches()
    verdict, reason_code = validator.VERDICT_RULES[verdict_rule_key]
    error_type = cast(
        Type[EmailValidationError],
        pydoc.locate(f"validate_email.exceptions.{verdict_rule_key}"),
    )
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        side_effect=error_type({}),
    ) as mock:
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        assert mock.call_count == 1
        assert result.validator is Validator.PY3_VALIDATE_EMAIL
        assert result.verdict is verdict
        assert result.reason is not None
        assert result.reason.code is reason_code


@given(
    address=emails().filter(is_parsable_address),
    verdict_rule_key=sampled_from(sorted(validator.VERDICT_RULES.keys())),
)
def test_that_validate_with_smtp_error_returns_expected_verdict(
    address: str, verdict_rule_key: str
):
    # Arrange
    clear_caches()
    verdict, reason_code = validator.VERDICT_RULES[verdict_rule_key]
    error_type = cast(
        Type[EmailValidationError],
        pydoc.locate(f"validate_email.exceptions.{verdict_rule_key}"),
    )
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        # First call will return True and it will interpreted as a valid address.
        # After that, SMTP validation will be performed and it will raise given error.
        side_effect=[True, error_type({})],
    ) as mock:
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        assert mock.call_count == 2
        assert result.validator is Validator.PY3_VALIDATE_EMAIL
        assert result.verdict is verdict
        assert result.reason is not None
        assert result.reason.code is reason_code


@given(address=emails().filter(is_parsable_address))
def test_that_validate_with_unhandled_validation_error_returns_risky_verdict(
    address: str,
):
    # Arrange
    clear_caches()
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        side_effect=UnhandledValidatorError(),
    ) as mock:
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        assert mock.call_count == 1
        assert result.validator is Validator.PY3_VALIDATE_EMAIL
        assert result.verdict is Verdict.RISKY
        assert result.reason is None


################################################################################
# ZeroBounce
################################################################################


@given(
    address=emails().filter(is_parsable_address),
    zerobounce_status_rule_key=sampled_from(ZBValidateStatus),
    zerobounce_sub_status=sampled_from(ZBValidateSubStatus),
)
def test_that_validate_with_invalid_address_returns_expected_zerobounce_verdict(
    address: str,
    zerobounce_status_rule_key: ZBValidateStatus,
    zerobounce_sub_status: ZBValidateSubStatus,
):
    # Arrange
    clear_caches()
    verdict = validator.ZEROBOUNCE_STATUS_RULES[zerobounce_status_rule_key]
    zerobounce_settings = ZeroBounceSettings(api_key=STUB_ZEROBOUNCE_API_KEY)
    zerobounce_response = ZBValidateResponse(None)
    zerobounce_response.status = zerobounce_status_rule_key
    zerobounce_response.sub_status = zerobounce_sub_status
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        return_value=True,
    ), patch.object(
        zerobouncesdk, "validate", return_value=zerobounce_response
    ) as mock:
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            zerobounce_settings,
        )
        # Assert
        assert mock.call_count == 1
        assert result.validator is Validator.ZEROBOUNCE
        assert result.verdict is verdict
        if verdict is Verdict.VALID:
            assert result.reason is None
        else:
            assert result.reason is not None
            assert result.reason.code == zerobounce_sub_status.name


################################################################################
# Suggestion
################################################################################


@given(address=emails().filter(is_parsable_address))
def test_that_validate_with_valid_address_returns_no_suggestion(address: str):
    # Arrange
    clear_caches()
    with patch.object(validate_email, "validate_email_or_fail", return_value=True):
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        assert result.suggestion is None


@given(
    address=emails().filter(is_parsable_address),
    wrong_domain=sampled_from(sorted(DOMAINS_WITH_SUGGESTION.keys())),
)
def test_that_validate_with_risky_address_returns_suggestion(
    address: str, wrong_domain: str
):
    # Arrange
    clear_caches()
    parsed_address = EmailAddress(address.lower())
    address = parsed_address.user + "@" + wrong_domain
    suggested_domain = DOMAINS_WITH_SUGGESTION[wrong_domain]
    with patch.object(validate_email, "validate_email_or_fail", return_value=None):
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        assert result.suggestion is not None
        parsed_suggestion = EmailAddress(result.suggestion)
        assert parsed_suggestion.user == parsed_address.user
        assert parsed_suggestion.domain == suggested_domain


@given(
    address=emails().filter(is_parsable_address),
    wrong_domain=sampled_from(sorted(DOMAINS_WITH_SUGGESTION.keys())),
)
def test_that_validate_with_invalid_address_returns_suggestion(
    address: str, wrong_domain: str
):
    # Arrange
    clear_caches()
    parsed_address = EmailAddress(address.lower())
    address = parsed_address.user + "@" + wrong_domain
    suggested_domain = DOMAINS_WITH_SUGGESTION[wrong_domain]
    with patch.object(
        validate_email,
        "validate_email_or_fail",
        side_effect=DNSTimeoutError(),
    ):
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        assert result.suggestion is not None
        parsed_suggestion = EmailAddress(result.suggestion)
        assert parsed_suggestion.user == parsed_address.user
        assert parsed_suggestion.domain == suggested_domain


@given(
    plain_text=text().filter(lambda t: "@" not in t)
)  # Avoids potential email addresses.
def test_that_validate_with_plain_text_returns_no_suggestion(plain_text: str):
    # Arrange
    clear_caches()
    # Act
    result = validator.validate(
        plain_text,
        get_stub_logger(),
        get_validator_settings(),
        get_zerobounce_settings(),
    )
    # Assert
    assert result.suggestion is None


@given(address=emails().filter(is_parsable_address))
def test_that_validate_with_suggestion_equal_to_address_returns_no_suggestion(
    address: str,
):
    # Arrange
    clear_caches()
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=None
    ), patch.object(
        pymailcheck, "suggest", return_value={"full": address.lower()}
    ) as mock:
        # Act
        result = validator.validate(
            address,
            get_stub_logger(),
            get_validator_settings(),
            get_zerobounce_settings(),
        )
        # Assert
        mock.assert_called_once()
        assert result.suggestion is None


################################################################################
# Settings
################################################################################


@given(
    address=emails().filter(is_parsable_address),
    check_format=booleans(),
)
def test_that_validate_uses_check_format_setting(address: str, check_format: bool):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(check_format=check_format)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=False
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        assert "check_format" in mock.call_args.kwargs
        assert mock.call_args.kwargs["check_format"] == check_format


@given(
    address=emails().filter(is_parsable_address),
    check_blacklist=booleans(),
)
def test_that_validate_uses_check_blacklist_setting(
    address: str, check_blacklist: bool
):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(check_blacklist=check_blacklist)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=False
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        assert "check_blacklist" in mock.call_args.kwargs
        assert mock.call_args.kwargs["check_blacklist"] == check_blacklist


@given(
    address=emails().filter(is_parsable_address),
    check_dns=booleans(),
)
def test_that_validate_uses_check_dns_setting(address: str, check_dns: bool):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(check_dns=check_dns)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=False
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        assert "check_dns" in mock.call_args.kwargs
        assert mock.call_args.kwargs["check_dns"] == check_dns


@given(
    address=emails().filter(is_parsable_address),
    dns_timeout=integers().filter(lambda t: t > 0),
)
def test_that_validate_uses_dns_timeout_setting(address: str, dns_timeout: int):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(dns_timeout=dns_timeout)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=False
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        assert "dns_timeout" in mock.call_args.kwargs
        assert mock.call_args.kwargs["dns_timeout"] == dns_timeout


@given(
    address=emails().filter(is_parsable_address),
    check_smtp=booleans(),
)
def test_that_validate_uses_check_smtp_setting(address: str, check_smtp: bool):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(check_smtp=check_smtp)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        expected_call_count = 2 if check_smtp else 1
        assert mock.call_count == expected_call_count
        first_call_args = mock.call_args_list[0]
        assert "check_smtp" in first_call_args.kwargs
        assert first_call_args.kwargs["check_smtp"] is False
        if check_smtp:
            second_call_args = mock.call_args_list[1]
            assert "check_smtp" in second_call_args.kwargs
            assert second_call_args.kwargs["check_smtp"] is True


@given(
    address=emails().filter(is_parsable_address),
    smtp_timeout=integers().filter(lambda t: t > 0),
)
def test_that_validate_uses_smtp_timeout_setting(address: str, smtp_timeout: int):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(smtp_timeout=smtp_timeout)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        assert "smtp_timeout" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_timeout"] == smtp_timeout


@given(
    address=emails().filter(is_parsable_address),
    smtp_helo_host=text(),
)
def test_that_validate_uses_smtp_helo_host_setting(address: str, smtp_helo_host: str):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(smtp_helo_host=smtp_helo_host)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        assert "smtp_helo_host" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_helo_host"] == smtp_helo_host


@given(
    address=emails().filter(is_parsable_address),
    smtp_from_address=text(),
)
def test_that_validate_uses_smtp_from_address_setting(
    address: str, smtp_from_address: str
):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(smtp_from_address=smtp_from_address)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        assert "smtp_from_address" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_from_address"] == smtp_from_address


@given(
    address=emails().filter(is_parsable_address),
    smtp_skip_tls=booleans(),
)
def test_that_validate_uses_smtp_skip_tls_setting(address: str, smtp_skip_tls: bool):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(smtp_skip_tls=smtp_skip_tls)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        assert "smtp_skip_tls" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_skip_tls"] == smtp_skip_tls


@given(
    address=emails().filter(is_parsable_address),
    smtp_debug=booleans(),
)
def test_that_validate_uses_smtp_debug_setting(address: str, smtp_debug: bool):
    # Arrange
    clear_caches()
    validator_settings = ValidatorSettings(smtp_debug=smtp_debug)
    with patch.object(
        validate_email, "validate_email_or_fail", return_value=True
    ) as mock:
        # Act
        validator.validate(
            address, get_stub_logger(), validator_settings, get_zerobounce_settings()
        )
        # Assert
        assert "smtp_debug" in mock.call_args.kwargs
        assert mock.call_args.kwargs["smtp_debug"] == smtp_debug
