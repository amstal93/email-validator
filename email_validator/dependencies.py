# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import functools
import hmac
import logging
from datetime import datetime, timezone
from functools import cache

import backoff
import requests
from fastapi import Depends, Security, status
from fastapi.exceptions import HTTPException
from fastapi.security import APIKeyHeader
from healthcheck import HealthCheck
from opencensus.ext.azure.log_exporter import AzureLogHandler
from opencensus.ext.azure.trace_exporter import AzureExporter
from opencensus.trace.samplers import AlwaysOffSampler, AlwaysOnSampler
from opencensus.trace.tracer import Tracer
from zerobouncesdk import zerobouncesdk

from email_validator.settings import (
    AppInsightsSettings,
    HealthCheckSettings,
    SecuritySettings,
    ValidatorSettings,
    ZeroBounceSettings,
)

API_KEY_HEADER_NAME = "X-Api-Key"

api_key_header_auth = APIKeyHeader(name=API_KEY_HEADER_NAME, auto_error=False)


def __get_validator_settings():
    return ValidatorSettings()


@cache
def get_validator_settings():
    return __get_validator_settings()


def __get_appinsights_settings():
    return AppInsightsSettings()


@cache
def get_appinsights_settings():
    return __get_appinsights_settings()


def __get_zerobounce_settings():
    return ZeroBounceSettings()


@cache
def get_zerobounce_settings():
    return __get_zerobounce_settings()


def __get_logger(appinsights_settings: AppInsightsSettings):
    logger = logging.getLogger(__name__)
    uvicorn_logger = logging.getLogger("uvicorn.error")

    # Copy log level from uvicorn logger, which is controlled via command line arguments.
    # If gunicorn is used, its log level has already been copied into uvicorn one.
    logger.setLevel(uvicorn_logger.level)

    # Use handlers of uvicorn logger, since they print beautiful messages to the console.
    # Moreover, by using the same list, additional handlers will be shared.
    logger.handlers = (
        uvicorn_logger.handlers
        or (
            # When running directly on unicorn, for example with dev server,
            # handlers are defined on "uvicorn" logger.
            uvicorn_logger.parent
            and uvicorn_logger.parent.name == "uvicorn"
            and uvicorn_logger.parent.handlers
        )
        or []
    )

    if appinsights_settings.instrumentation_key:
        logger.addHandler(
            AzureLogHandler(connection_string=appinsights_settings.connection_string)
        )

    return logger


@cache
def get_logger(
    appinsights_settings: AppInsightsSettings = Depends(get_appinsights_settings),
):
    return __get_logger(appinsights_settings)


def __get_tracer(appinsights_settings: AppInsightsSettings):
    if appinsights_settings.instrumentation_key:
        return Tracer(
            exporter=AzureExporter(
                connection_string=appinsights_settings.connection_string
            ),
            sampler=AlwaysOnSampler(),
        )

    return Tracer(
        exporter=None,
        sampler=AlwaysOffSampler(),
    )


def get_tracer(
    appinsights_settings: AppInsightsSettings = Depends(get_appinsights_settings),
):
    return __get_tracer(appinsights_settings)


def __get_security_settings():
    return SecuritySettings()


@cache
def get_security_settings():
    return __get_security_settings()


async def get_api_key(
    api_key_header: str = Security(api_key_header_auth),
    logger: logging.Logger = Depends(get_logger),
    security_settings: SecuritySettings = Depends(get_security_settings),
):
    if not security_settings.enabled:
        # Security has been disabled; therefore, no check on API keys has to be performed.
        return None

    if not api_key_header:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Missing API key"
        )

    # Step 1: look for an API key with received value.
    matched_api_keys = filter(
        # When validating an API key, timing attacks can be mitigated
        # by avoiding to compare strings with the "==" operator.
        lambda ak: hmac.compare_digest(ak.value, api_key_header),
        security_settings.api_keys,
    )

    # Step 2: make sure that filtered API keys have not expired.
    matched_api_keys = filter(
        lambda ak: ak.expires_at is None or ak.expires_at >= datetime.now(timezone.utc),
        matched_api_keys,
    )

    matched_api_keys = list(matched_api_keys)

    if not matched_api_keys:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid API key",
        )

    matched_api_key = matched_api_keys[0]
    logger.info('Validated an API key with name "%s"', matched_api_key.name)

    return matched_api_key


def __get_health_check_settings():
    return HealthCheckSettings()


@cache
def get_health_check_settings():
    return __get_health_check_settings()


@backoff.on_exception(backoff.expo, requests.RequestException, max_tries=3)
def __ping_website(
    website: str,
    logger: logging.Logger,
    health_check_settings: HealthCheckSettings,
):
    """Returns True if specified website responds to an HTTP HEAD request."""
    response = requests.head(
        website,
        timeout=health_check_settings.ping_timeout,
        allow_redirects=True,
    )

    if response.status_code < 400:
        return (True, "Ping is OK")

    logger.warning(
        "Ping to '%s' returned %d. Response headers: %s",
        website,
        response.status_code,
        response.headers,
    )
    return (False, "Ping is KO")


def __check_zerobounce_credits(
    logger: logging.Logger, zerobounce_settings: ZeroBounceSettings
):
    zerobounce_response = zerobouncesdk.get_credits()
    available_credits = int(zerobounce_response.credits)
    credits_threshold = zerobounce_settings.credits_threshold

    if available_credits >= credits_threshold:
        return True, "Credits check is OK"

    logger.warning(
        "Available ZeroBounce credits (%s) are below the configured threshold (%s)",
        available_credits,
        credits_threshold,
    )
    return False, f"Credits check is KO ({available_credits}/{credits_threshold})"


def __get_health_check(
    logger: logging.Logger,
    health_check_settings: HealthCheckSettings,
    zerobounce_settings: ZeroBounceSettings,
):
    health_check = HealthCheck(failed_status=status.HTTP_503_SERVICE_UNAVAILABLE)

    for i, ip_address in enumerate(health_check_settings.ping_websites, start=1):
        check = functools.partial(
            __ping_website, ip_address, logger, health_check_settings
        )
        setattr(check, "__name__", f"ping_website_{i}")
        health_check.add_check(check)

    if zerobounce_settings.api_key:
        check = functools.partial(
            __check_zerobounce_credits, logger, zerobounce_settings
        )
        setattr(check, "__name__", "check_zerobounce_credits")
        health_check.add_check(check)

    return health_check


@cache
def get_health_check(
    logger: logging.Logger = Depends(get_logger),
    health_check_settings: HealthCheckSettings = Depends(get_health_check_settings),
    zerobounce_settings: ZeroBounceSettings = Depends(get_zerobounce_settings),
):
    return __get_health_check(logger, health_check_settings, zerobounce_settings)
