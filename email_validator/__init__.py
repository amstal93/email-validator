# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import configparser

import pymailcheck

parser = configparser.ConfigParser()
parser.read("pyproject.toml")
__version__ = parser["tool.poetry"]["version"].replace('"', "")

__all__ = ["__version__"]

CUSTOM_PYMAILCHECK_DOMAINS = pymailcheck.DOMAINS.union(
    [
        # Additional Italian free email service providers:
        "alice.it",
        "libero.it",
        "tiscali.it",
    ]
)
