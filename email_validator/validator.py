# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from functools import lru_cache

import pymailcheck
import validate_email
from validate_email.email_address import AddressFormatError, EmailAddress
from validate_email.exceptions import (
    AddressNotDeliverableError,
    DNSConfigurationError,
    DNSTimeoutError,
    DomainBlacklistedError,
    DomainNotFoundError,
    EmailValidationError,
    NoMXError,
    NoNameserverError,
    NoValidMXError,
    SMTPCommunicationError,
    SMTPTemporaryError,
    TLSNegotiationError,
)
from zerobouncesdk import zerobouncesdk
from zerobouncesdk.zb_validate_status import ZBValidateStatus

from email_validator import CUSTOM_PYMAILCHECK_DOMAINS
from email_validator.models import (
    AddressValidationResult,
    Validator,
    Verdict,
    VerdictReason,
)
from email_validator.settings import ValidatorSettings, ZeroBounceSettings

VERDICT_RULES = {
    # Format related errors:
    AddressFormatError.__name__: (Verdict.INVALID, "invalid_address_format"),
    # Domain and DNS related errors:
    DomainBlacklistedError.__name__: (Verdict.INVALID, "domain_blacklisted"),
    DomainNotFoundError.__name__: (Verdict.INVALID, "domain_not_found"),
    NoNameserverError.__name__: (Verdict.INVALID, "no_nameserver_found"),
    DNSTimeoutError.__name__: (Verdict.INVALID, "dns_timeout"),
    DNSConfigurationError.__name__: (Verdict.INVALID, "dns_configuration_error"),
    NoMXError.__name__: (Verdict.INVALID, "no_mx_record"),
    NoValidMXError.__name__: (Verdict.INVALID, "no_valid_mx_record"),
    # SMTP related errors:
    AddressNotDeliverableError.__name__: (Verdict.INVALID, "address_not_deliverable"),
    SMTPCommunicationError.__name__: (Verdict.INVALID, "smtp_communication_error"),
    SMTPTemporaryError.__name__: (Verdict.RISKY, "smtp_temporary_error"),
    TLSNegotiationError.__name__: (Verdict.RISKY, "tls_negotiation_error"),
}

ZEROBOUNCE_STATUS_RULES = {
    ZBValidateStatus.valid: Verdict.VALID,
    ZBValidateStatus.invalid: Verdict.INVALID,
    ZBValidateStatus.catch_all: Verdict.VALID,
    ZBValidateStatus.unknown: Verdict.RISKY,
    ZBValidateStatus.spamtrap: Verdict.INVALID,
    ZBValidateStatus.abuse: Verdict.INVALID,
    ZBValidateStatus.do_not_mail: Verdict.INVALID,
}


@lru_cache
def validate(
    address: str,
    logger: logging.Logger,
    validator_settings: ValidatorSettings,
    zerobounce_settings: ZeroBounceSettings,
) -> AddressValidationResult:
    address = address.lower()
    logger.info('Validating email address "%s"', address)

    # Try to parse email address, so that we can expose account and domain information.
    parsed_address, user, domain = __parse_address(address, logger)

    # Validation is performed in two steps: a cheap one and an expensive one.
    # Following "cheap" validation checks that email address is formally correct
    # and that domain is correctly configured (it does exist, it has MX records, ...).
    # If this check passes, then the "expensive" one is performed.
    validator, verdict, reason = __perform_formal_validation(
        address, logger, validator_settings
    )

    # Validation is performed in two steps: a cheap one and an expensive one.
    # Following "expensive" validation checks that email address actually exists.
    # It relies on ZeroBounce, if API key has been specified, or it falls back
    # to "py3-validate-email" library, which talks to the SMTP servers of given email domain.
    if validator_settings.check_smtp and verdict is Verdict.VALID:
        if zerobounce_settings.api_key:
            validator, verdict, reason = __perform_zerobounce_validation(
                address, logger
            )
        else:
            validator, verdict, reason = __perform_smtp_validation(
                address, logger, validator_settings
            )
    else:
        logger.info(
            'Email address "%s" is %s, SMTP validation is not necessary',
            address,
            verdict,
        )

    # If verdict is not valid and given address is formally correct,
    # then we can try to suggest the user the correct address
    # by checking it against common email providers.
    suggestion = None
    if verdict is not Verdict.VALID and parsed_address is not None:
        suggest_result = pymailcheck.suggest(
            address, domains=CUSTOM_PYMAILCHECK_DOMAINS
        )
        if suggest_result and suggest_result["full"] != address:
            suggestion = suggest_result["full"]

    logger.info(
        'Email address "%s" has been validated with verdict "%s",'
        ' reason code "%s" and suggestion "%s"',
        address,
        verdict,
        reason.code if reason is not None else "",
        suggestion if suggestion is not None else "",
    )
    return AddressValidationResult(
        address=address,
        user=user,
        domain=domain,
        validator=validator,
        verdict=verdict,
        reason=reason,
        suggestion=suggestion,
    )


def __parse_address(address: str, logger: logging.Logger):
    try:
        parsed_address = EmailAddress(address)
        user, domain = parsed_address.user, parsed_address.domain
        logger.debug(
            'Email address "%s" has been parsed: user "%s" and domain "%s"',
            address,
            user,
            domain,
        )
    except AddressFormatError:
        logger.warning('Email address "%s" could not be parsed', address, exc_info=True)
        parsed_address = None
        user, domain = None, None

    return parsed_address, user, domain


def __handle_validation_error(
    error: EmailValidationError, address: str, logger: logging.Logger
):
    logger.warning(
        'An error was raised while validating email address "%s". %s',
        address,
        error,
        exc_info=True,
    )

    error_type_name = type(error).__name__
    if error_type_name in VERDICT_RULES:
        verdict, reason_code = VERDICT_RULES[error_type_name]
        reason = VerdictReason(code=reason_code, message=error.__str__())
        return verdict, reason

    return Verdict.RISKY, None


def __perform_formal_validation(address, logger, validator_settings):
    try:
        is_valid = validate_email.validate_email_or_fail(
            address,
            check_format=validator_settings.check_format,
            check_blacklist=validator_settings.check_blacklist,
            check_dns=validator_settings.check_dns,
            dns_timeout=validator_settings.dns_timeout,
            check_smtp=False,  # SMTP check is performed in the next step.
        )
        verdict = Verdict.VALID if is_valid else Verdict.RISKY
        reason = None
    except EmailValidationError as error:
        verdict, reason = __handle_validation_error(error, address, logger)

    return Validator.PY3_VALIDATE_EMAIL, verdict, reason


def __perform_smtp_validation(address, logger, validator_settings):
    logger.info(
        'Performing SMTP validation with email address "%s" with py3-validate-email',
        address,
    )

    try:
        is_valid = validate_email.validate_email_or_fail(
            address,
            check_format=False,
            check_blacklist=False,
            check_dns=False,
            check_smtp=True,
            smtp_timeout=validator_settings.smtp_timeout,
            smtp_helo_host=validator_settings.smtp_helo_host,
            smtp_from_address=validator_settings.smtp_from_address,
            smtp_skip_tls=validator_settings.smtp_skip_tls,
            smtp_debug=validator_settings.smtp_debug,
        )
        verdict = Verdict.VALID if is_valid else Verdict.RISKY
        reason = None
    except EmailValidationError as error:
        verdict, reason = __handle_validation_error(error, address, logger)

    return Validator.PY3_VALIDATE_EMAIL, verdict, reason


def __perform_zerobounce_validation(address, logger):
    logger.info(
        'Performing SMTP validation with email address "%s" with ZeroBounce',
        address,
    )

    zerobounce_response = zerobouncesdk.validate(address)
    verdict = ZEROBOUNCE_STATUS_RULES[zerobounce_response.status]

    if verdict is not Verdict.VALID and zerobounce_response.sub_status:
        reason = VerdictReason(
            code=zerobounce_response.sub_status.name,
            message=zerobounce_response.error,
        )
    else:
        reason = None

    return Validator.ZEROBOUNCE, verdict, reason
