# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging

from fastapi import Depends, FastAPI, Request, Security
from fastapi.responses import RedirectResponse, Response
from opencensus.trace import config_integration
from opencensus.trace.attributes_helper import COMMON_ATTRIBUTES
from opencensus.trace.span import SpanKind
from uvicorn.workers import UvicornWorker
from zerobouncesdk import zerobouncesdk

from email_validator import __version__, validator
from email_validator.dependencies import (
    get_api_key,
    get_appinsights_settings,
    get_health_check,
    get_logger,
    get_tracer,
    get_validator_settings,
    get_zerobounce_settings,
)
from email_validator.models import AddressValidationResult
from email_validator.settings import ValidatorSettings, ZeroBounceSettings


class CustomUvicornWorker(UvicornWorker):
    CONFIG_KWARGS = {
        "access_log": False,
        "server_header": False,
    }


API_TITLE = "Email Validator"
API_DESCRIPTION = """
Web service which exposes endpoints to validate email addresses.

[OpenAPI docs for v1 endpoints](/api/v1/docs)
"""

HTTP_METHOD = COMMON_ATTRIBUTES["HTTP_METHOD"]
HTTP_PATH = COMMON_ATTRIBUTES["HTTP_PATH"]
HTTP_ROUTE = COMMON_ATTRIBUTES["HTTP_ROUTE"]
HTTP_STATUS_CODE = COMMON_ATTRIBUTES["HTTP_STATUS_CODE"]
HTTP_URL = COMMON_ATTRIBUTES["HTTP_URL"]

config_integration.trace_integrations(["logging"])

app = FastAPI(
    title=API_TITLE,
    description=API_DESCRIPTION,
    version=__version__,
    redoc_url=None,
)


@app.on_event("startup")
async def on_startup():
    appinsights_settings = get_appinsights_settings()
    logger = get_logger(appinsights_settings)
    if appinsights_settings.instrumentation_key:
        logger.info("Application Insights Instrumentation Key has been specified")

    zerobounce_settings = get_zerobounce_settings()
    if zerobounce_settings.api_key:
        logger.info("ZeroBounce API key has been specified")
        zerobouncesdk.initialize(zerobounce_settings.api_key)


@app.get("/", include_in_schema=False)
@app.get("/redoc", include_in_schema=False)
@app.get("/swagger", include_in_schema=False)
async def redirect_to_docs():
    return RedirectResponse("/docs")


@app.get(
    "/health",
    tags=["health_check"],
    operation_id="get_health",
)
def get_health(health_check=Depends(get_health_check)):
    """Verifies if the web service is healthy."""
    content, status_code, headers = health_check.run()
    return Response(content=content, status_code=status_code, headers=headers)


@app.get(
    "/version",
    tags=["version"],
    operation_id="get_version",
)
async def get_version():
    """Responds the semantic version of the web service."""
    return __version__


v1 = FastAPI(
    title=API_TITLE,
    description=API_DESCRIPTION,
    version=__version__,
    redoc_url=None,
)


@v1.middleware("http")
async def trace_http_request(request: Request, call_next):
    appinsights_settings = get_appinsights_settings()
    tracer = get_tracer(appinsights_settings)

    with tracer.span("main") as span:
        # Set attributes available before the request is evaluated.
        span.span_kind = SpanKind.SERVER
        tracer.add_attribute_to_current_span(
            attribute_key=HTTP_METHOD, attribute_value=request.method
        )
        tracer.add_attribute_to_current_span(
            attribute_key=HTTP_URL, attribute_value=str(request.url)
        )
        tracer.add_attribute_to_current_span(
            attribute_key=HTTP_PATH, attribute_value=request.url.path
        )
        tracer.add_attribute_to_current_span(
            attribute_key=HTTP_ROUTE, attribute_value=request.url.path
        )

        # Evaluate the request.
        status_code = 500
        try:
            response = await call_next(request)
            status_code = response.status_code
        finally:
            # Set attributes available after the request has been evaluated.
            tracer.add_attribute_to_current_span(
                attribute_key=HTTP_STATUS_CODE, attribute_value=status_code
            )

    return response


@v1.get(
    "/validate/",
    tags=["email_validator"],
    operation_id="validate",
    response_model=AddressValidationResult,
    dependencies=[Security(get_api_key)],
)
def validate(
    address: str,
    logger: logging.Logger = Depends(get_logger),
    validator_settings: ValidatorSettings = Depends(get_validator_settings),
    zerobounce_settings: ZeroBounceSettings = Depends(get_zerobounce_settings),
):
    """Validates specified email address."""
    return validator.validate(address, logger, validator_settings, zerobounce_settings)


app.mount("/api/v1", v1)
